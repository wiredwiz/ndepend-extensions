﻿// <Name>Avoid types with too many methods</Name>
warnif count > 0 from t in JustMyCode.Types 

  // Optimization: Fast discard of non-relevant types 
  where t.Methods.Count() > 20 &&
    !t.HasAttribute("Org.Edgerunner.NDepend.Attributes.MethodCountOkAttribute".AllowNoMatch())

  // Don't match these methods
  let methods = t.Methods.Where(
       m => !(m.IsGeneratedByCompiler ||
              m.IsConstructor || m.IsClassConstructor ||
              m.IsPropertyGetter || m.IsPropertySetter ||
              m.IsEventAdder || m.IsEventRemover))

  where methods.Count() > 20 
  orderby methods.Count() descending

  let isStaticWithNoMutableState = (t.IsStatic && t.Fields.Any(f => !f.IsImmutable))
  let staticFactor = (isStaticWithNoMutableState ? 0.2 : 1)

select new { 
   t, 
   nbMethods = methods.Count(),
   instanceMethods = methods.Where(m => !m.IsStatic), 
   staticMethods = methods.Where(m => m.IsStatic),

   t.NbLinesOfCode,

   Debt = (staticFactor*methods.Count().Linear(20, 1, 200, 10)).ToHours().ToDebt(),

   // The annual interest varies linearly from interest for severity major for 30 methods
   // to interest for severity critical for 200 methods
   AnnualInterest = (staticFactor*methods.Count().Linear(
                              20,  Severity.Minor.AnnualInterestThreshold().Value.TotalMinutes, 
                              200, Severity.Critical.AnnualInterestThreshold().Value.TotalMinutes)).ToMinutes().ToAnnualInterest()
}

//<Description>
// This rule matches types with more than 20 methods. 
// Such type might be hard to understand and maintain.
//
// Notice that methods like constructors or property 
// and event accessors are not taken account.
//
// Having many methods for a type might be a symptom
// of too many responsibilities implemented.
//
// Maybe you are facing the **God Class** phenomenon:
// A **God Class** is a class that controls way too many other classes 
// in the system and has grown beyond all logic to become 
// *The Class That Does Everything*.
//</Description>

//<HowToFix>
// To refactor properly a *God Class* please read *HowToFix advices* 
// from the default rule **Types to Big**.
////
// The estimated Debt, which means the effort to fix such issue,
// varies linearly from 1 hour for a type with 20 methods,
// up to 10 hours for a type with 200 or more methods.
//
// In Debt and Interest computation, this rule takes account of the fact 
// that static types with no mutable fields are just a collection of 
// static methods that can be easily splitted and moved from one type 
// to another.
//</HowToFix>